Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kbackup
Source: https://invent.kde.org/utilities/kbackup
Upstream-Contact: Martin Koller <kollix@aon.at>

Files: *
Copyright: 2006-2018 Martin Koller, kollix@aon.at
            2018 Martin Schlander <mschlander@opensuse.org>
            2018 Stelios <sstavra@gmail.com>
            2018 Steve Allewell <steve.allewell@gmail.com>
            2018-2019 Iñigo Salvador Azurmendi <xalba@euskalnet.net>
            2018-2019 Tommi Nieminen <translator@legisign.org>
            2018-2019 Adrián Chaves (Gallaecio) <adrian@chaves.io>
            2013, 2018 Giovanni Sora <g.sora@tiscali.it>
            2018 Luigi Toscano <luigi.toscano@tiscali.it>
            2018 Japanese KDE translation team <kde-jp@kde.org>
            2019 Jung Hee Lee <daemul72@gmail.com>
            2019 Shinjo Park <kde@peremen.name>
            2018 Freek de Kruijf <freekdekruijf@kde.nl>
            2018 Jan Mussche <jan.mussche@gmail.com>
            2018 Øystein Steffensen-Alværvik <ystein@posteo.net>
            2018, 2019 Karl Ove Hufthammer <karl@huftis.org>
            2018, 2019 Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
            2006, 2007, 2009 Carlos Gonçalves <mail@cgoncalves.info>
            2009 Márcio Moraes <marciopanto@gmail.com>
            2018-2020 Luiz Fernando Ranghetti <elchevive@opensuse.org>
            2019 Guo Yunhe (guoyunhe)
            2019 Chinese <kde-i18n-doc@kde.org>
            2019 Marek Laane <qiilaq69@gmail.com>
            2019 British English <kde-l10n-en_gb@kde.org>
            2019 Norwegian Nynorsk <l10n-no@lister.huftis.org>
            2019, 2020 Matjaž Jeran <matjaz.jeran@amis.net>
License: GPL-2

Files: po/ca@valencia/kbackup.po
       po/ca/kbackup.po
       po/uk/kbackup.po
Copyright: 2018 Antoni Bella Pérez <antonibella5@yahoo.com>
           2018 Yuri Chornoivan <yurchor@ukr.net>
License: LGPL-2.1+3+KDEeV

Files: po/ca/docs/*
       po/de/docs/*
       po/nl/docs/*
       po/sv/docs/*
       po/uk/docs/*
       doc/en/*
Copyright: 2006-2018 Martin Koller <kollix@aon.at>
       2018 Stefan Asserhäll <stefan.asserhall@bredband.net>
       2018 Freek de Kruijf <freekdekruijf@kde.nl>
       2018 Юрій Чорноіван <yurchor@ukr.net>
License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation
 License version 1.2 can be found in "/usr/share/common-licenses/GFDL-1.2".

Files: po/cs/kbackup.po
       po/de/kbackup.po
       po/es/kbackup.po
       po/fr/kbackup.po
       po/pt/kbackup.po
       po/ru/kbackup.po
       po/sk/kbackup.po
       po/sv/kbackup.po
Copyright: 2010-2011 Pavel Fric <fripohled.blogspot.com>
       2018 Vit Pelcak <vit@pelcak.org>
       2006-2010 Martin Koller <kollix@aon.at>
       2018  Burkhard Lück <lueck@hube-lueck.de>
       2018 Frederik Schwarzer <schwarzer@kde.org>
       2018 Eloy Cuadra <ecuadra@eloihr.net>
       2007 gerardo <gejobj@gmail.com>
       2018 Víctor Rodrigo Córdoba <vrcordoba@gmail.com>
       2006 regis Floret <r.floret@laposte.net>
       2006 Martin Koller <koller@etm.at>
       2006-2011 Alain Portal <alain.portal@univ-montp2.fr>
       2018 Simon Depiets <sdepiets@gmail.com>
       2006, 2007, 2009 Carlos Gonçalves <mail@cgoncalves.info>
       2006- 2009 Alexey Kouznetsov <Alexey.Kouznetsov@GMail.com>
       2018 Alexander Potashev <aspotashev@gmail.com>
       2007, 2009 Jozef Riha <jose1711@gmail.com>
       2018 Roman Paholik <wizzardsk@gmail.com>
       2019 Mthw <jari_45@hotmail.com>
       2007 Mikael Martinsson <mikael.martinsson@jamtnet.se>
       2018 Stefan Asserhäll <stefan.asserhall@bredband.net>
License: public-domain
 No license required for any purpose; the work is not subject to
 copyright in any jurisdiction.
 .
 This file is put in the public domain.

Files: src/org.kde.kbackup.appdata.xml
Copyright: 2006-2018 Martin Koller, kollix@aon.at
License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is, without
 any warranty.

Files: debian/*
Copyright: 2018-2019 Scarlett Moore <sgmoore@kde.org>
           2010 Scott Kitterman <scott@kitterman.com>
License: GPL-2

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+3+KDEeV
 LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".
 .
 LGPL-3
 This package is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this package; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-3'.
 .
 KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
